package exceptions;

public class ImproperFileFormatException extends Exception {
	public ImproperFileFormatException(String message){
		super(message);
	}
}