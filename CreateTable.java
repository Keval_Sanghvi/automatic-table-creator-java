import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import javax.swing.JOptionPane;
import java.io.File;
import java.io.FileNotFoundException;
import exceptions.*;

class CreateTable {

	Connection conn;
	FileReader fileReader;
	String fileContent;
	Table table;

	CreateTable() {
		this.conn = MySQLConnect.connectDB();
	}

	public void createTable() {
		try {
			Statement stmt = conn.createStatement();
			String sql = table.getCreateTableQuery();
         	stmt.executeUpdate(sql);
         	JOptionPane.showMessageDialog(null, "Table Creation Successful!");
		} catch(SQLException e) {
			JOptionPane.showMessageDialog(null, "Table Creation Failed! " + e);
		}
	}

	public static void main(String[] args) {
		try {
			String fileName = args[0];
			fileName = fileName.replace(".json", "");
			File file = new File("migrations\\" + fileName + ".json");
			if(!file.exists()) {
				throw new FileNotFoundException();
			}
			CreateTable myTable = new CreateTable();
			myTable.fileReader = new FileReader(file);
			myTable.fileContent = myTable.fileReader.fileRead();
			if(myTable.fileContent.isEmpty()) {
				throw new EmptyFileException("File Provided Is Empty!");
			}
			JSONParser jsonParser = new JSONParser(myTable.fileContent);
			jsonParser.parseJSON();
			myTable.table = jsonParser.getTableObject();
			myTable.createTable();
			MySQLConnect.closeConnection();
		} catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("No File Name Provided!");
		} catch(FileNotFoundException e) {
			System.out.println("File Name Provided Does Not Exist In Migrations Folder!");
		} catch(EmptyFileException e) {
			System.out.println("File Provided Is Empty!");
		} catch(ImproperFileFormatException e) {
			System.out.println("File Format Of Specified JSON File Is Improper!\nCheck File Format Once Again!\nFile May Contain Extra Commas!");
		} catch(Exception e) {
			System.out.println("Error!");
		}
	}
}