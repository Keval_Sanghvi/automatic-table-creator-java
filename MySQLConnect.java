import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import java.util.HashMap;

public class MySQLConnect {
    
    static Connection conn;
    // java -cp ".;mysql.jar;" CreateTable

    public static Connection connectDB() {
        HashMap<String, String> hashMap;
        FileReader fileReader = new FileReader("config.ini");
        String fileContent = fileReader.fileRead();
        hashMap = fileReader.simplifyConfigFile(fileContent);
        try {
            MySQLConnect.conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + hashMap.get("database_name"), hashMap.get("username"), hashMap.get("password"));
            return conn;
        } catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "Connection Failed! " + e);
            return null;
        }
    }

    public static void closeConnection() {
        try {
            MySQLConnect.conn.close();
        } catch(SQLException e) {
            JOptionPane.showMessageDialog(null, "An Error Occured While Closing Connection! " + e);
        }
    }
}
