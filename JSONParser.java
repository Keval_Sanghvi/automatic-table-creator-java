import java.util.regex.*;
import exceptions.ImproperFileFormatException;

class JSONParser {

	String fileContent;
	Table table;
	static final String COLUMN = "column";
	static final String REFERENCE_TABLE = "reference_table";
	static final String REFERENCE_COLUMN = "reference_column";
	static final String ON_DELETE = "on_delete";
	static final String ON_UPDATE = "on_update";

	JSONParser(String fileContent) {
		this.fileContent = fileContent;
		this.table = new Table();
	}

	public void parseJSON() throws ImproperFileFormatException {
		try {
			parseTableName();
			parseColumns();
			parseDataTypes();
			parsePrimaryKey();
			parseDefaultValues();
			parseNotNullValues();
			parseForeignKey();
		} catch(ArrayIndexOutOfBoundsException e) {
			throw new ImproperFileFormatException("Format of JSON File is Improper!");
		}
	}

	public void parseTableName() {
		String patternString = "(\"table_name\":\\s)(.*?)(,)";
        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(fileContent);
        if(matcher.find()) {
            table.setTableName(removeDoubleQuotesFromString(matcher.group(2)));
        }
	}

	public void parseColumns() {
		String patternString = "(\"columns\": \\{)(.*?)(\"datatype\"|\"constraint\"|\"default\")";
        Pattern pattern = Pattern.compile(patternString, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(fileContent);
        if(matcher.find()) {
            String []values = matcher.group(2).split(",");
            for(int i = 0; i < values.length - 1; i++) {
            	values[i] = values[i].trim();
            	String []columns = values[i].split(":\\s");
            	table.addColumnName(removeDoubleQuotesFromString(columns[1]));
            }
        }
	}

	public void parseDataTypes() {
		String patternString = "(\"datatype\": \\{)(.*?)(\\})";
        Pattern pattern = Pattern.compile(patternString, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(fileContent);
        if(matcher.find()) {
            String []values = matcher.group(2).split(",");
            for(int i = 0; i < values.length; i++) {
            	String []columns = values[i].split(":\\s");
            	table.addDataType(removeDoubleQuotesFromString(columns[0]), removeDoubleQuotesFromString(columns[1]));
            }
        }
	}

	public void parsePrimaryKey() {
		String autoIncrementId = parseAutoIncrementId();
		if(!autoIncrementId.isEmpty()) {
			table.setPrimaryKey(autoIncrementId);
			table.setIsAutoIncrementId(true);
			return;
		}
		table.setIsAutoIncrementId(false);
		String patternString = "(\"primary_key\":\\s)(.*?)(,|\\})";
		Pattern pattern = Pattern.compile(patternString, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(fileContent);
		if(matcher.find()) {
			table.setPrimaryKey(removeDoubleQuotesFromString(matcher.group(2)));
		}
	}

	public String parseAutoIncrementId() {
		String patternString = "(\"auto_increment\":\\s)(.*?)(,)";
		Pattern pattern = Pattern.compile(patternString);
		Matcher matcher = pattern.matcher(fileContent);
		if(matcher.find()) {
			return removeDoubleQuotesFromString(matcher.group(2));
		}
		return "";
	}

	public void parseDefaultValues() {
		String patternString = "(\"default\":\\s\\{)(.*?)(\\})";
		Pattern pattern = Pattern.compile(patternString, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(fileContent);
		if(matcher.find()) {
		    String []values = matcher.group(2).split(",");
		    for(int i = 0; i < values.length; i++) {
		    	values[i] = values[i].trim();
		    	String []columns = values[i].split(":\\s");
		    	table.addDefaultValue(removeDoubleQuotesFromString(columns[0]), removeDoubleQuotesFromString(columns[1]));
		    }
		}
	}

	public void parseNotNullValues() {
		String patternString = "(\"not_null\":\\s\\{)(.*?)(\\})";
		Pattern pattern = Pattern.compile(patternString, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(fileContent);
		if(matcher.find()) {
			String []values = matcher.group(2).split(",");
			for(int i = 0; i < values.length; i++) {
				values[i] = values[i].trim();
				String []columns = values[i].split(":\\s");
				table.addNotNullValue(removeDoubleQuotesFromString(columns[1]));
			}
		}
	}

	public void parseForeignKey() {
		String patternString = "(\"foreign_key\":\\s\\{)(.*?)(\\})";
		Pattern pattern = Pattern.compile(patternString, Pattern.DOTALL);
		Matcher matcher = pattern.matcher(fileContent);
		if(matcher.find()) {
			String []values = matcher.group(2).split(",");
			for(int i = 0; i < values.length; i++) {
				values[i] = values[i].trim();
				String []columns = values[i].split(":\\s");
				table.setIsForeignKey(true);
				if(removeDoubleQuotesFromString(columns[0]).equals(JSONParser.COLUMN)) {
					table.setForeignKeyColumn(removeDoubleQuotesFromString(columns[1]));
				} else if(removeDoubleQuotesFromString(columns[0]).equals(JSONParser.REFERENCE_TABLE)) {
					table.setReferencingTableName(removeDoubleQuotesFromString(columns[1]));
				} else if(removeDoubleQuotesFromString(columns[0]).equals(JSONParser.REFERENCE_COLUMN)) {
					table.setReferencingColumnName(removeDoubleQuotesFromString(columns[1]));
				} else if(removeDoubleQuotesFromString(columns[0]).equals(JSONParser.ON_DELETE)) {
					table.setOnDelete(removeDoubleQuotesFromString(columns[1]));
				} else if(removeDoubleQuotesFromString(columns[0]).equals(JSONParser.ON_UPDATE)) {
					table.setOnUpdate(removeDoubleQuotesFromString(columns[1]));
				}
			}
		}
	}

	public String removeDoubleQuotesFromString(String input) {
		input = input.trim();
		return input.replaceAll("\"", "");
	}

	public Table getTableObject() {
		return this.table;
	}
}