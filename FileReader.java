import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;

class FileReader {
	File file;

	FileReader(File file) {
		this.file = file;
	}

	FileReader(String fileName) {
		this.file = new File(fileName);
	}

	String fileRead() {
		try(FileInputStream in = new FileInputStream(file)) {
			String result = "";
			int b;
			while((b=in.read())!=-1) {
				result += (char)b;
			}
			return result;
		} catch(Exception e) {
			System.out.println("Error!");
		}
		return "";
	}

	HashMap<String, String> simplifyConfigFile(String str) {
        HashMap<String, String> hashMap = new HashMap<>();
        String arr[] = str.split(",");
        for(int i = 0; i < arr.length; i++){
            String keyVal[] = arr[i].split(":");
            String key = keyVal[0].trim();
            String value = keyVal[1].trim();
            hashMap.put(key, value);
        }
        return hashMap;
    }
}