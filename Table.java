import java.util.ArrayList;
import java.util.HashMap;

class Table {
	private String tableName;
	private ArrayList<String> columnNames;
	private HashMap<String, String> dataTypes;
	private HashMap<String, String> defaultValues;
	private ArrayList<String> notNullValues;
	private String primaryKey;
	private boolean isAutoIncrementId;
	private boolean isForeignKey;
	private String foreignKeyColumn;
	private String referencingTableName;
	private String referencingColumnName;
	private String onDelete = "CASCADE";
	private String onUpdate = "CASCADE";

	Table() {
		this.columnNames = new ArrayList<>();
		this.dataTypes = new HashMap<>();
		this.defaultValues = new HashMap<>();
		this.notNullValues = new ArrayList<>();
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void addColumnName(String column) {
		this.columnNames.add(column);
	}

	public void addDataType(String column, String dataType) {
		this.dataTypes.put(column, dataType);
	}

	public void addDefaultValue(String column, String defaultvalue) {
		this.defaultValues.put(column, defaultvalue);
	}

	public void addNotNullValue(String column) {
		this.notNullValues.add(column);
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}

	public void setIsAutoIncrementId(boolean value) {
		this.isAutoIncrementId = value;
	}

	public void setIsForeignKey(boolean value) {
		this.isForeignKey = value;
	}

	public void setForeignKeyColumn(String foreignKeyColumn) {
		this.foreignKeyColumn = foreignKeyColumn;
	}

	public void setReferencingTableName(String referencingTableName) {
		this.referencingTableName = referencingTableName;
	}

	public void setReferencingColumnName(String referencingColumnName) {
		this.referencingColumnName = referencingColumnName;
	}

	public void setOnDelete(String onDelete) {
		this.onDelete = onDelete;
	}

	public void setOnUpdate(String onUpdate) {
		this.onUpdate = onUpdate;
	}

	public String getTableName() {
		return this.tableName;
	}

	public ArrayList<String> getColumnNames() {
		return this.columnNames;
	}

	public HashMap<String, String> getDataTypes() {
		return this.dataTypes;
	}

	public HashMap<String, String> getDefaultValues() {
		return this.defaultValues;
	}

	public ArrayList<String> getNotNullColumns() {
		return this.notNullValues;
	}

	public String getPrimaryKey() {
		return this.primaryKey;
	}

	public boolean getIsAutoIncrementId() {
		return this.isAutoIncrementId;
	}

	public boolean getIsForeignKey() {
		return this.isForeignKey;
	}

	public String getForeignKeyColumn() {
		return this.foreignKeyColumn;
	}

	public String getReferencingTableName() {
		return this.referencingTableName;
	}

	public String getReferencingColumnName() {
		return this.referencingColumnName;
	}

	public String getOnDelete() {
		return this.onDelete;
	}

	public String getOnUpdate() {
		return this.onUpdate;
	}

	public String getCreateTableQuery() {
		String sql = "CREATE TABLE " + this.tableName + " (";
		if(isAutoIncrementId) {
			sql = sql + this.primaryKey + " int(11) NOT NULL AUTO_INCREMENT, ";
		}
		for(String column: columnNames) {
			if(defaultValues.containsKey(column) && notNullValues.contains(column)) {
				sql = sql + column + " " + dataTypes.get(column) + " DEFAULT '" + defaultValues.get(column) + "' " + "NOT NULL, ";
			} else if(defaultValues.containsKey(column)) {
				sql = sql + column + " " + dataTypes.get(column) + " DEFAULT '" + defaultValues.get(column) + "', ";
			} else if(notNullValues.contains(column)) {
				sql = sql + column + " " + dataTypes.get(column) + " NOT NULL, ";
			} else {
				sql = sql + column + " " + dataTypes.get(column) + ", ";
			}
		}
		if(isForeignKey) {
			sql = sql + "FOREIGN KEY(" + foreignKeyColumn + ") REFERENCES " + referencingTableName + "(" + referencingColumnName + ") ON DELETE " + onDelete + " ON UPDATE " + onUpdate + ", "; 
		}
		sql = sql + "PRIMARY KEY(" + this.primaryKey + "))";
		return sql;
	}
}