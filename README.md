**This JAVA project will automatically create tables in the database (provided the database exists). 
User should create the JSON file with the given format for creating tables in migrations folder.
config.ini File includes configuration of the database.
For creating tables, user has to specify the file name which has to be migrated as database table in the following way:**

- Commands:

**java -cp ".;mysql.jar;" CreateTable fileName**


- JSON FORMAT :

{
    "table": {
        "table_name": "demo",
        "columns": {
            "column": "name",
            "column": "email",
            "column": "telephone",
            "column": "age",
            "datatype": {
                "name": "varchar(255)",
                "email": "varchar(255)",
                "telephone": "int(11)",
                "age": "int(11)"
            },
            "constraint": {
                "primary_key": "email"
            },
            "not_null": {
                "column": "name"
            }
        }
    }
}

OR

{
    "table": {
        "table_name": "demo1",
        "auto_increment": "id",
        "columns": {
            "column": "name",
            "column": "email",
            "column": "telephone",
            "column": "age",
            "datatype": {
                "name": "varchar(255)",
                "email": "varchar(255)",
                "telephone": "int(11)",
                "age": "int(11)"
            },
            "not_null": {
                "column": "name",
                "column": "telephone"
            }
        }
    }
}

OR

{
    "table": {
        "table_name": "demo2",
        "columns": {
            "column": "name",
            "column": "email",
            "column": "telephone",
            "column": "age",
            "datatype": {
                "name": "varchar(255)",
                "email": "varchar(255)",
                "telephone": "int(11)",
                "age": "int(11)"
            },
            "constraint": {
                "primary_key": "email"
            },
            "default": {
                "age": 18,
                "name": "John"
            },
            "not_null": {
                "column": "name",
                "column": "email"
            }
        }
    }
}

OR

**Foreign Key Table JSON File**

- {
    "table": {
        "table_name": "demo",
        "auto_increment": "id",
        "columns": {
            "column": "name",
            "column": "email",
            "column": "age",
            "datatype": {
                "name": "varchar(255)",
                "email": "varchar(255)",
                "age": "int(11)"
            },
            "not_null": {
                "column": "name"
            }
        }
    }
}

- {
    "table": {
        "table_name": "demo3",
        "auto_increment": "id",
        "columns": {
            "column": "contact",
            "column": "demo_id",
            "datatype": {
                "contact": "int(11)",
                "demo_id": "int(11)"
            },
            "constraint": {
                "primary_key": "email",
                "foreign_key": {
                    "column": "demo_id",
                    "reference_table": "demo",
                    "reference_column": "id",
                    "on_delete": "cascade",
                    "on_update": "cascade"
                }
            },
            "not_null": {
                "column": "contact"
            }
        }
    }
}

